import React from 'react';
import {View, Button} from 'react-native';

export const CalculateButton = ({title, press, style}) => {
  return (
    <View style={style}>
      <Button
        color="#ffffff"
        title={title}
        onPress={press}
      >
        {title}
      </Button>
    </View>
  )
};
