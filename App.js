import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import {CalculateButton} from "./components/Button/Button";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 50
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  display: {
    color: 'white',
    flex: 1,
    textAlign: 'center',
    fontSize: 24
  },
  displayRow: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default class App extends React.Component {
  state = {
    display: '',
    buttonsRowFirst: ['1', '2', '3', '+'],
    buttonsRowSecond: ['4', '5', '6', '-'],
    buttonsRowThird: ['7', '8', '9', '*'],
    buttonsRowForth: ['0', '.', '/', '<'],
    buttonsRowFifths: ['CE', '='],
  };

  displayInfo = (value) => {
    if (value === '<' && this.state.display.length !== 0) {
      const display = this.state.display;
      let displayArray = display.split('');
      displayArray.pop();
      displayArray = displayArray.join('');

      this.setState({display: displayArray});
    } else if (value === '=') {
      try {
        const display = eval(this.state.display);
        this.setState({display : display + ''});
      } catch (e) {
        this.setState({display : 'Error!'});
      }
    } else if (value === 'CE') {
      this.setState({display: ''});
    } else {
      const display = this.state.display + value;
      this.setState({display});
    }
  };

  returnButtonColor = (button, index) => {
    if (button === '=' || button === '+' || button === '-' || button === '*' || button === '/' || button === '<' || button === 'CE') {
      return (
        <CalculateButton
          title={button} key={index}
          style={{
            backgroundColor: '#f4a041',
            flex: 1,
            borderColor: 'white',
            borderStyle: 'solid',
            borderWidth: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          press={() => this.displayInfo(button)}
        />
      )
    } else {
      return (
        <CalculateButton
          title={button} key={index}
          style={{
            backgroundColor: '#418bf4',
            flex: 1,
            borderColor: 'white',
            borderStyle: 'solid',
            borderWidth: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          press={() => this.displayInfo(button)}
        />
      )
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.displayRow}>
          <Text style={styles.display}>{this.state.display}</Text>
        </View>
        <View style={styles.row}>
          {this.state.buttonsRowFirst.map((button, index) => (this.returnButtonColor(button, index)))}
        </View>
        <View style={styles.row}>
          {this.state.buttonsRowSecond.map((button, index) => (this.returnButtonColor(button, index)))}
        </View>
        <View style={styles.row}>
          {this.state.buttonsRowThird.map((button, index) => (this.returnButtonColor(button, index)))}
        </View>
        <View style={styles.row}>
          {this.state.buttonsRowForth.map((button, index) => (this.returnButtonColor(button, index)))}
        </View>
        <View style={styles.row}>
          {this.state.buttonsRowFifths.map((button, index) => (this.returnButtonColor(button, index)))}
        </View>
      </View>
    );
  }
}
